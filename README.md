# README

### Ресурсы:
1. Answer - ответы

  Столбцы:
  > - [x] answer - text (ответ)

### Модели:
1. Level - уровень знания языка

  Столбцы:
  > - [x] title - string (название)
  > - [x] themes - belongs_to (темы)

  Связи:
  > - [x] themes (has_many)

1. Theme - мема вопросов

  Столбцы:
  > - [x] title - string (название)
  > - [x] units - belongs_to (юниты)

1. Unit - Юнит

  Столбцы:
  > - [x] title - string (название)
  > - [x] theory - text (теория)
  > - [x] exercises - belongs_to (практика/упражнения)

  Связи:
  > - [x] exercises (has_many)

1. Exercise - задание

  Столбцы:
  > - [x] title - string (содержание задания)
  > - [x] sentences - belongs_to (предложение/задание)

  Связи:
  > - [x] sentences (has many)

1. Sentence - предложение

  Столбцы:
  > - [x] text - text (текст теории или картинка)
  > - [x] have_specific_answer - boolean (имеется ли конкретный ответ) default: false
  > - [x] answers - belongs_to (ответ на вопрос предложения)

  Связи:
  > - [x] answers (has many)

1. Answer - ответы

  Столбцы:
  > - [x] answer - text (ответ)
