require "rspec_api_documentation_helper"

RSpec.resource "Users" do
  header "Content-Type", "application/vnd.api+json"

  post "/v1/users" do
    parameter "type", <<~DESC, required: true
      The type of the resource. Must be users.
    DESC

    let "type" do
      "users"
    end

    parameter "firstname", <<~DESC, scope: :attributes, required: true
      The firstname of the user.
    DESC

    let "firstname" do
      "John"
    end

    parameter "lastname", <<~DESC, scope: :attributes, required: true
      The lastname of the user.
    DESC

    let "lastname" do
      "Smith"
    end

    parameter "email", <<~DESC, scope: :attributes, required: true
      The email of the user.
    DESC

    let "email" do
      "example@mail.com"
    end

    parameter "password", <<~DESC, scope: :attributes, required: true
      The password of the user.
    DESC

    let "password" do
      "12344321"
    end

    example_request "POST /v1/users" do
      expect(status).to eq 201
      user = JSON.parse(response_body)
      expect(user["data"]["attributes"]["email"]).to eq send("email")
    end
  end
end
