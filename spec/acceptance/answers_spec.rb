require "rspec_api_documentation_helper"

RSpec.resource "Answers" do
  header "Content-Type", "application/vnd.api+json"

  shared_context "answers parameters" do
    parameter "type", <<~DESC, required: true
      The type of the resource. Must be 'answers'.
    DESC

    let "type" do
      "answers"
    end

    parameter "text", <<~DESC, required: true, scope: :attributes
      The text of the answer.
    DESC
  end

  shared_context "persisted answer" do
    let! :persisted_answer do
      FactoryGirl.create(:answer)
    end

    let :answer_id do
      persisted_answer.id.to_s
    end

    parameter "id", "The id of the resource", required: true

    let "id" do
      answer_id
    end
  end

  post "/v1/answers" do

    include_context "answers parameters"

    let "text" do
      "am"
    end

    example_request "POST /v1/answers" do
      explanation "Используется для создания ответа на предложение. (Разрешено только для пользователей с определенной ролью)"
      expect(status).to eq 201
    end
  end

  patch "/v1/answers/:answer_id" do

    include_context "persisted answer"

    include_context "answers parameters"

    let "text" do
      "are"
    end

    example_request "PATCH /v1/answers/:id" do
      explanation "Используется для изменения ответа на предложение. (Разрешено только для пользователей с определенной ролью)"

      expect(status).to eq 200
      expect(JSON.parse(response_body)["data"]["attributes"]["text"]).to eq public_send("text")
    end
  end

  get "/v1/answers/:answer_id" do

    include_context "persisted answer"

    example_request "GET /v1/answers/:id" do
      explanation "Используется для получения ответа на предложение."
      expect(status).to eq 200
    end
  end

  delete "/v1/answers/:answer_id" do

    include_context "persisted answer"

    example_request "DELETE /v1/answers/:id" do
      explanation "Используется для удаления ответа на предложение. (Разрешено только для пользователей с определенной ролью)"
      expect(status).to eq 204
    end
  end
end
