require "rspec_api_documentation_helper"

RSpec.resource "Themes" do
  header "Content-Type", "application/vnd.api+json"

  shared_context "units parameters" do
    parameter "type", <<~DESC, required: true
      The type of the resource. Must be 'units'.
    DESC

    let "type" do
      "units"
    end

    parameter "title", <<~DESC, scope: :attributes, required: true
      The title of the unit.
    DESC

    parameter "theory", <<~DESC, scope: :attributes, required: true
      The theory of the unit.
    DESC

  end
end
