require "rspec_api_documentation_helper"

RSpec.resource "Theory" do
  header "Content-Type", "application/vnd.api+json"

  shared_context "theory parameters" do
    parameter "type", <<~DESC, required: true
      The type of the resource. Always 'theories'.
    DESC

    let "type" do
      "theories"
    end

    parameter "body", <<~DESC, scope: :attributes, required: true
      The body of the theory.
    DESC
  end

  post "/v1/theories" do
    include_context "theory parameters"

    let! :valid_base64_image do
      Base64.encode64 File.read(Rails.root.join("public","images","image1.png"))
    end

    let "body" do
      "data:image/png;base64,#{valid_base64_image}"
    end

    example_request "POST /v1/theories" do
      expect(status).to eq 201

      body = JSON.parse(response_body)
                  .fetch("data")
                  .fetch("attributes")
                  .fetch("body")

      expect(body["url"]).to be_present
    end
  end

  shared_context "persisted theories" do
    let! :persisted_theory do
      FactoryGirl.create(:theory)
    end

    let :theory_id do
      persisted_theory.id.to_s
    end
  end

  patch "/v1/theories/:theory_id" do
    include_context "theory parameters"
    include_context "persisted theories"
    
    parameter "id", "The id of the resource", required: true

    let "id" do
      persisted_theory.id.to_s
    end

    let! :new_valid_base64_image do
      Base64.encode64 File.read(Rails.root.join("public","images","image2.png"))
    end

    let "body" do 
      "data:image/png;base64,#{new_valid_base64_image}"
    end

    example_request "PATCH /v1/theories/:id" do
      expect(status).to eq 200
    end
  end

  get "/v1/theories/:theory_id" do
    include_context "persisted theories"

    example_request "GET /v1/theories/:id" do
      expect(status).to eq 200
    end
  end
  
  delete "/v1/theories/:theory_id" do
    include_context "persisted theories"

    example_request "DELETE /v1/theories/:id" do
      expect(status).to eq 204
    end
  end
end
