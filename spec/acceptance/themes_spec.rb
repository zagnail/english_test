require "rspec_api_documentation_helper"

RSpec.resource "Themes" do
  header "Content-Type", "application/vnd.api+json"

  shared_context "themes parameters" do
    parameter "type", <<~DESC, required: true
      The type of the resource. Must be 'themes'.
    DESC

    let "type" do
      "themes"
    end

    parameter "title", <<~DESC, scope: :attributes, required: true
      The title of the theme.
    DESC

    parameter "book", <<~DESC, scope: :relationships, required: true
      The book to which this theme is made.
    DESC
  end

  shared_context "persisted theme" do
    let! :persisted_theme do
      FactoryGirl.create(:theme, title: "Present Continuous")
    end

    let :theme_id do
      persisted_theme.id.to_s
    end

    parameter "id", "The id of the resource", require: true

    let "id" do
      theme_id
    end
  end

  post "/v1/themes" do
    include_context "themes parameters"

    let "title" do
      "Present Simple"
    end

    # let! :persisted_unit do
    #   FactoryGirl.create(:unit)
    # end
    #
    # let "unit" do
    #   {
    #     data: {
    #       id: persisted_unit.id.to_s,
    #       type: "units"
    #     }
    #   }
    # end

    let! :persisted_book do
      FactoryGirl.create(:book)
    end

    let "book" do
      {
        data: {
          id: persisted_book.id.to_s,
          type: "books"
        }
      }
    end

    example_request "POST /v1/themes" do
      expect(status).to eq 201
    end
  end

  patch "/v1/themes/:theme_id" do

    include_context "persisted theme"

    include_context "themes parameters"

    let "title" do
      "Present Perfect"
    end

    example_request "PATCH /v1/themes/:id" do
      expect(status).to eq 200
      expect(JSON.parse(response_body)["data"]["attributes"]["title"]).to eq public_send("title")
    end
  end

  get "/v1/themes" do
    example_request "GET /v1/themes" do
      expect(status).to eq 200
      expect(JSON.parse(response_body)["data"].size).to eq Theme.count
    end
  end

  get "/v1/themes/:theme_id" do

    include_context "persisted theme"

    example_request "GET /v1/themes/:id" do
      expect(status).to eq 200
    end
  end

  delete "/v1/themes/:theme_id" do

    include_context "persisted theme"

    example_request "DELETE /v1/themes/:id" do
      expect(status).to eq 204
    end
  end
end
