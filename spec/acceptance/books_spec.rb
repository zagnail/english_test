require "rake"
require "rspec_api_documentation_helper"

RSpec.resource "Books" do
  header "Content-Type", "application/vnd.api+json"

  before do
    EnglishTest::Application.load_tasks
    Rake::Task["seed:books"].execute
  end

  shared_context "books parameters" do
    parameter "type", <<~DESC, required: true
      The type of the resource. Must be 'books'
    DESC

    let "type" do
      "books"
    end

    parameter "title", <<~DESC, required: true, scope: :attributes
      The title of the book.
    DESC

    parameter "description", <<~DESC, scope: :attributes
      The description of the book.
    DESC
  end

  post "/v1/books" do
    include_context "books parameters"

    let "title" do
      "English Phrasal Verbs"
    end

    let "description" do
      "English Phrasal Verbs in Use Advanced is a vocabulary book for advanced learners.\nIt is primarily designed as a self-study reference and practice book but it can also be used for classroom work. The book covers many phrasal verbs useful to students preparing for the Cambridge CAE, CPE and IELTS examinations."
    end

    example_request "POST /v1/books" do
      expect(status).to eq 201
    end
  end

  patch "/v1/books/:book_id" do
    let! :persisted_book do
      FactoryGirl.create(:book, title: "English Idioms in Use: Advanced", description: "This book presents and practises over 1000 of the most useful and frequent idioms in typical contexts. This reference and practice book looks at the most colourful and fun area of English vocabulary - idioms. This book will appeal to students at advanced level who want to understand and use the English really used by native speakers, and students preparing for higher level exams, such as CAE, CPE and IELTS. Over 1,000 of the most useful and frequent idioms, which learners are likely to encounter are presented and practised in typical contexts, so that learners using this book will have hundreds of idioms \"at their fingertips\".")
    end

    let :book_id do
      persisted_book.id.to_s
    end

    parameter "id", "The if of the resource.", required: true

    let "id" do
      book_id
    end

    include_context "books parameters"

    let "title" do
      "English Collocations in Use: Advanced"
    end

    let "description" do
      "Covers all the most useful collocations (common word combinations) at Advanced level to help make your English more fluent and natural-sounding. English Collocations in Use Advanced presents and practises hundreds of collocations in typical contexts to help you improve your written and spoken English. It also includes tips on learning strategies and ways to avoid common learner errors. The book is informed by the Cambridge International Corpus to make sure that the collocations taught are the most frequent and useful for students at each level."
    end

    example_request "PATCH /v1/books/:id" do
      expect(status).to eq 200
      expect(JSON.parse(response_body)["data"]["attributes"]["title"]).to eq public_send("title")
      expect(JSON.parse(response_body)["data"]["attributes"]["description"]).to eq public_send("description")
    end
  end

  get "/v1/books" do
    example_request "GET /v1/books" do
      expect(status).to eq 200
      expect(JSON.parse(response_body)["data"].size).to eq Book.count
    end
  end

  get "/v1/books/:book_id" do
    let :book_id do
      Book.first.id.to_s
    end

    example_request "GET /v1/books/:id" do
      expect(status).to eq 200
    end
  end

  delete "/v1/books/:book_id" do
    let :book_id do
      Book.first.id.to_s
    end

    example_request "DELETE /v1/books/:id" do
      expect(status).to eq 204
    end
  end
end
