RSpec.describe "OAuth password flow" do

  let :email do
    "example@mail.com"
  end

  let :password do
    "12344321"
  end

  let! :user do
    FactoryGirl.create(:user, email: email, password: password)
  end

  it "creates a token and returns it when the creadentials are valid" do
    params = { grant_type: "password", username: email, password: password }
    post "/oauth/token", params: params
    expect(response.status).to eq 200
  end

  it "does not issue a token if the credentials are invalid" do
    params = { grant_type: "password", username: email, password: "another_password" }
    post "/oauth/token", params: params
    expect(response.status).to eq 401
  end

end
