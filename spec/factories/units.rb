FactoryGirl.define do
  factory :unit do
    title "am/is/are"
    theory "I am\nHe/She/It is\nWe/You/They are"
    association :exercise, factory: :exercise, strategy: :build
  end
end
