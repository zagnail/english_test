FactoryGirl.define do
  factory :theory do
    body "data:image/png;base64,#{Base64.encode64(File.read(Rails.root.join("public", "images", "image1.png")))}"
  end
end
