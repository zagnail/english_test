FactoryGirl.define do
  factory :user do
    firstname "Firstname"
    lastname "Lastname"
    password "12344321"
    sequence (:email) { |n| "example#{n}@mail.com" }
  end
end
