FactoryGirl.define do
  factory :theme do
    title "Present Continuous"
    association :book, factory: :book, strategy: :build
  end
end
