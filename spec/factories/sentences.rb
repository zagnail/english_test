FactoryGirl.define do
  factory :sentence do
    text "I {} student"
    has_specific_answer true
    association :answer, factory: :answer, strategy: :build
  end
end
