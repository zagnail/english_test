FactoryGirl.define do
  factory :exercise do
    title "Write am, is or are."
    association :sentences, factory: :sentence, strategy: :build
  end
end
