RSpec.describe Theme, type: :model do
  # subject { FactoryGirl.create :theme }

  it { is_expected.to have_attribute :title }

  it { is_expected.to have_many :units }

  it { is_expected.to belong_to :book }

  it { is_expected.to validate_presence_of :title }

  it { is_expected.to validate_presence_of :book }

  # it { is_expected.to validate_uniqueness_of :title }
end
