RSpec.describe Answer do
  it { is_expected.to have_attribute :text }

  it { is_expected.to validate_presence_of :text }

  it { is_expected.to have_many :sentences }
end
