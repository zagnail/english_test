RSpec.describe Book do
  subject { FactoryGirl.create :book }

  it { is_expected.to have_attribute :title }

  it { is_expected.to have_attribute :description }

  it { is_expected.to have_many :themes }

  it { is_expected.to have_many :units }

  it { is_expected.to validate_presence_of :title }

  it { is_expected.to validate_presence_of :description }

  it { is_expected.to validate_uniqueness_of :title }
end
