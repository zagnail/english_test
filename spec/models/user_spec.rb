RSpec.describe User do
  %w(firstname lastname email).each do |attribute|
    it { is_expected.to have_attribute attribute }
    it { is_expected.to validate_presence_of attribute }
  end

  it { is_expected.to validate_presence_of :password }

  it "validates the uniqueness of email" do
    subject = FactoryGirl.build(:user)
    subject.save!
  end

  it "persists a password digest based on the password that can be used for authentication" do
    password = "password"
    subject = FactoryGirl.create(:user, password: password)
    expect(subject.authenticate(password)).to eq subject
  end
end
