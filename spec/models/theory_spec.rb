RSpec.describe Theory, type: :model do
  it { is_expected.to have_attribute :body } 

  it { is_expected.to validate_presence_of :body }

end
