module V1
  class ThemeResource < BaseResource
    attribute :title

    has_one :book
    # has_many :units
  end
end
