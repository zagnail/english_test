module V1
  class BookResource < BaseResource
    attribute :title
    attribute :description
  end
end
