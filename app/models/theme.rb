class Theme < ApplicationRecord
  has_many :units
  belongs_to :book

  validates :title, presence: true
  validates :book, presence: true
end
