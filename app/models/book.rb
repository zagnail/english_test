class Book < ApplicationRecord
  has_many :themes

  has_many :units, through: :themes

  validates :title, uniqueness: true, presence: true

  validates :description, presence: true
end
