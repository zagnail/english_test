class Answer < ApplicationRecord
  has_many :sentences

  validates :text, presence: true
end
