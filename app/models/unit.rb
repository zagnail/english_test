class Unit < ApplicationRecord
  has_many :exercises

  belongs_to :theme

  belongs_to :theory

  has_one :book, through: :theme

  validates :title, presence: true

  validates :theory, presence: true

  validates :theme, presence: true
end
