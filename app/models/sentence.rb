class Sentence < ApplicationRecord
  belongs_to :answer

  belongs_to :exercise

  validates :text, presence: true

  validates :answer, presence: true

  validates :exercise, presence: true
end
