class Exercise < ApplicationRecord
  has_many :sentences

  belongs_to :unit

  validates :title, presence: true

  validates :unit, presence: true
end
