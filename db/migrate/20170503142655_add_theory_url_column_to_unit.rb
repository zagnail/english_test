class AddTheoryUrlColumnToUnit < ActiveRecord::Migration[5.0]
  def change
    add_column :units, :theory_id, :integer
    add_column :units, :theory_url, :string, default: "/default_theory.png"
  end
end
