class CreateExercises < ActiveRecord::Migration[5.0]
  def change
    create_table :exercises do |t|
      t.string :title, null: false
      t.belongs_to :sentences, index: true, foreing_key: true, null: false

      t.timestamps null: false
    end
  end
end
