class AddExerciseToSentences < ActiveRecord::Migration[5.0]
  def change
    add_reference :sentences, :exercise, index: true, foreing_key: true, null: false
  end
end
