class CreateTheories < ActiveRecord::Migration[5.0]
  def change
    create_table :theories do |t|
      t.string :body
      t.timestamps null: false
    end
  end
end
