class CreateThemes < ActiveRecord::Migration[5.0]
  def change
    create_table :themes do |t|
      t.string :title, null: false
      t.belongs_to :units, foreign_key: true

      t.timestamps null: false
    end
  end
end
