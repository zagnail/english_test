class AddThemeToLevel < ActiveRecord::Migration[5.0]
  def change
    add_reference :themes, :level, index: true, foreing_key: true, null: false
  end
end
