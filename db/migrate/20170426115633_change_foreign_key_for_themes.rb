class ChangeForeignKeyForThemes < ActiveRecord::Migration[5.0]
  def change
    rename_column :themes, :level_id, :book_id
  end
end
