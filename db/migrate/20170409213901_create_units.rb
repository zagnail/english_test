class CreateUnits < ActiveRecord::Migration[5.0]
  def change
    create_table :units do |t|
      t.string :title, null: false
      t.text :theory, null: false
      t.belongs_to :exercises, index: true, foreign_key: true, null: false

      t.timestamps null: false
    end
  end
end
