class AddUnitToExercises < ActiveRecord::Migration[5.0]
  def change
    add_reference :exercises, :unit, index: true, foreing_key: true, null: false
  end
end
