class AddThemeToUnits < ActiveRecord::Migration[5.0]
  def change
    add_reference :units, :theme, index: true, foreing_key: true, null: false
  end
end
