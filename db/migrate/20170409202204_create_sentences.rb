class CreateSentences < ActiveRecord::Migration[5.0]
  def change
    create_table :sentences do |t|
      t.text :text, null: false
      t.boolean :has_specific_answer, null: false, default: true
      t.belongs_to :answer, foreign_key: true

      t.timestamps null: false
    end
  end
end
