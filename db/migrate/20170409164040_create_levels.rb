class CreateLevels < ActiveRecord::Migration[5.0]
  def change
    create_table :levels do |t|
      t.string :title, null: false

      t.timestamps null: false
    end
  end
end
