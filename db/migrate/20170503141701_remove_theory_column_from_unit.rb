class RemoveTheoryColumnFromUnit < ActiveRecord::Migration[5.0]
  def change
    remove_column :units, :theory
  end
end
