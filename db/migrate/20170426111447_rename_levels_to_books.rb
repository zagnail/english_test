class RenameLevelsToBooks < ActiveRecord::Migration[5.0]
  def change
    rename_table :levels, :books
  end
end
