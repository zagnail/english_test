namespace :seed do
  task :books => :environment do
    YAML.load_file(Rails.root.join("db", "seeds", "books.yml")).each do |attribute|
      Book.find_or_create_by!(title: attribute["title"], description: attribute["description"])
    end
  end
end
